public class Application{

	public static void main(String[] args){
		
		//Student 1
		Student firstStudent=new Student();
		firstStudent.firstName="Tommy";
		firstStudent.lastName="Tran";
		firstStudent.grade=90;
		firstStudent.gender="male";
		
		
		//Student 2
		Student secondStudent=new Student();
		secondStudent.firstName="Noah";
		secondStudent.lastName="IForgot";
		secondStudent.grade=80;
		secondStudent.gender="Female";
		
		//print
		System.out.println(firstStudent.firstName);
		System.out.println(firstStudent.lastName);
		System.out.println(firstStudent.grade);
		System.out.println(firstStudent.gender);
		
		System.out.println(secondStudent.firstName);
		System.out.println(secondStudent.lastName);
		System.out.println(secondStudent.grade);
		System.out.println(secondStudent.gender);
		
		//methods
		firstStudent.fullName();
		secondStudent.fullName();
		System.out.println("Inside method gradePlusBonus: "+firstStudent.gradePlusBonus(7));
		System.out.println("Inside method gradePlusBonus: "+secondStudent.gradePlusBonus(5));
		
		//Array
		Student[] section3=new Student[3];
		section3[0]=firstStudent;
		section3[1]=secondStudent;
		section3[2]=new Student();
		
		section3[2].firstName="Emmanuelle";
		section3[2].lastName="What's her last name?";
		section3[2].grade=95;
		section3[2].gender="female";
		
		//print from array
		System.out.println(section3[0].firstName);
		
		System.out.println(section3[2].firstName);
		System.out.println(section3[2].lastName);
		System.out.println(section3[2].grade);
		System.out.println(section3[2].gender);
		
	}
}